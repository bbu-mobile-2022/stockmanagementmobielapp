package com.dinsaren.stockmanagementmobileapp.models.res;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class LoginRes{

	@SerializedName("phone")
	private String phone;

	@SerializedName("roles")
	private List<String> roles;

	@SerializedName("id")
	private int id;

	@SerializedName("accessToken")
	private String accessToken;

	@SerializedName("type")
	private String type;

	@SerializedName("email")
	private String email;

	@SerializedName("refreshToken")
	private String refreshToken;

	@SerializedName("username")
	private String username;

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setRoles(List<String> roles){
		this.roles = roles;
	}

	public List<String> getRoles(){
		return roles;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setRefreshToken(String refreshToken){
		this.refreshToken = refreshToken;
	}

	public String getRefreshToken(){
		return refreshToken;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"LoginRes{" + 
			"phone = '" + phone + '\'' + 
			",roles = '" + roles + '\'' + 
			",id = '" + id + '\'' + 
			",accessToken = '" + accessToken + '\'' + 
			",type = '" + type + '\'' + 
			",email = '" + email + '\'' + 
			",refreshToken = '" + refreshToken + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}