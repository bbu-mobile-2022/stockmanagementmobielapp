package com.dinsaren.stockmanagementmobileapp.models.res;

import com.google.gson.annotations.SerializedName;

public class BaesRes{

	@SerializedName("data")
	private Object data;

	@SerializedName("message")
	private String message;

	public void setData(Object data){
		this.data = data;
	}

	public Object getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"BaesRes{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}