package com.dinsaren.stockmanagementmobileapp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dinsaren.stockmanagementmobileapp.R;
import com.dinsaren.stockmanagementmobileapp.app.BaseActivity;
import com.dinsaren.stockmanagementmobileapp.ui.setup.category.CategoryActivity;
import com.dinsaren.stockmanagementmobileapp.utils.local.UserSharedPreference;

public class MainActivity extends BaseActivity {
    private TextView tvUsername;
    private Button btnSignOut, btnOpenCategory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSignOut = findViewById(R.id.btnSignOut);
        btnOpenCategory = findViewById(R.id.btnOpenCategory);
        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserSharedPreference.removeUser(MainActivity.this);
                Intent intent = new Intent(MainActivity.this,
                        LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });
        btnOpenCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        CategoryActivity.class);
                startActivity(intent);
            }
        });

    }

}