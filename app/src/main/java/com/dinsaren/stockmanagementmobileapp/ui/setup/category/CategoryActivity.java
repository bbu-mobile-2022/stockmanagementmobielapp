package com.dinsaren.stockmanagementmobileapp.ui.setup.category;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.dinsaren.stockmanagementmobileapp.R;
import com.dinsaren.stockmanagementmobileapp.adapters.CategoryAdapter;
import com.dinsaren.stockmanagementmobileapp.app.BaseActivity;
import com.dinsaren.stockmanagementmobileapp.models.res.Category;
import com.dinsaren.stockmanagementmobileapp.presenters.CategoryPresenter;
import com.dinsaren.stockmanagementmobileapp.utils.local.UserSharedPreference;
import com.dinsaren.stockmanagementmobileapp.views.CategoryView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class CategoryActivity extends BaseActivity implements CategoryView {
    private FloatingActionButton floatingActionButton;
    private RecyclerView recyclerViewCategory;
    private ProgressBar progressBar;
    private CategoryPresenter categoryPresenter;
    private CategoryAdapter categoryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        recyclerViewCategory = findViewById(R.id.recyclerviewCategory);
        progressBar = findViewById(R.id.progressBar);
        floatingActionButton = findViewById(R.id.floatingActionButton);
        categoryPresenter = new CategoryPresenter(this);
        categoryPresenter.getAllCategory(
                UserSharedPreference.getToken(this)
        );
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryActivity.this,
                        FormCategoryActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onLoading(String message) {
//        showMessage("Loading..");
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading(String message) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(String message) {
        showMessage("Success..");
    }

    @Override
    public void onError(String message) {
        showMessage("Success..");
    }

    @Override
    public void onServerError(String message) {
        showMessage(message);
    }

    @Override
    public void setData(List<Category> list) {
        if(!list.isEmpty()){
            categoryAdapter = new CategoryAdapter(this, list);
            LinearLayoutManager layoutManager = new LinearLayoutManager(
                    this, RecyclerView.VERTICAL,false
            );
            recyclerViewCategory.setLayoutManager(layoutManager);
            recyclerViewCategory.setAdapter(categoryAdapter);
        }
        showMessage(list.toString());

    }
}