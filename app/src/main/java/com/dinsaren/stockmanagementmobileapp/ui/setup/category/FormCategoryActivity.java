package com.dinsaren.stockmanagementmobileapp.ui.setup.category;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.dinsaren.stockmanagementmobileapp.R;
import com.dinsaren.stockmanagementmobileapp.app.BaseActivity;
import com.dinsaren.stockmanagementmobileapp.presenters.FormCategoryPresenter;
import com.dinsaren.stockmanagementmobileapp.utils.local.UserSharedPreference;
import com.dinsaren.stockmanagementmobileapp.views.FormCategoryView;

public class FormCategoryActivity extends BaseActivity implements FormCategoryView {
    private EditText etName,etNameKh;
    private Button btnCreate, btnBack;
    private ProgressBar progressBar;
    private FormCategoryPresenter formCategoryPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_category);
        btnBack = findViewById(R.id.btnBackCategory);
        etName =findViewById(R.id.etCategoryNameEn);
        etNameKh = findViewById(R.id.etCategoryNameKh);
        btnCreate = findViewById(R.id.btnCreateCategory);
        progressBar = findViewById(R.id.progressBar);
        formCategoryPresenter = new FormCategoryPresenter(this);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameEn = etName.getText().toString().trim();
                String nameKh = etNameKh.getText().toString().trim();
                if(nameKh.equals("")){
                    showMessage("Category name khmer is empty");
                    return;
                }
                if(nameEn.equals("")){
                    showMessage("Category name is empty");
                    return;
                }
                formCategoryPresenter.createCategory(nameEn, nameKh,
                        UserSharedPreference.getToken(
                                FormCategoryActivity.this));
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onLoading(String message) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading(String message) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(String message) {

    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onServerError(String message) {
        showMessage(message);
    }

    @Override
    public void onCreateSuccess() {
        showMessage("Create Success");
        finish();
    }
}