package com.dinsaren.stockmanagementmobileapp.views;

import com.dinsaren.stockmanagementmobileapp.models.res.LoginRes;

public interface LoginView extends BaseView{
    void onLoginSuccess(LoginRes res);
}
