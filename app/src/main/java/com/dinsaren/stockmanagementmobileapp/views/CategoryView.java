package com.dinsaren.stockmanagementmobileapp.views;

import com.dinsaren.stockmanagementmobileapp.models.res.Category;

import java.util.List;

public interface CategoryView extends BaseView {
    void setData(List<Category> list);
}
