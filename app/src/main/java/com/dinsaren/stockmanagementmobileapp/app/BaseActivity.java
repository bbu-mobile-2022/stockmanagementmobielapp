package com.dinsaren.stockmanagementmobileapp.app;

import android.content.Intent;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.dinsaren.stockmanagementmobileapp.models.User;
import com.dinsaren.stockmanagementmobileapp.ui.MainActivity;
import com.dinsaren.stockmanagementmobileapp.utils.local.UserSharedPreference;

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onResume() {
        super.onResume();

    }

    protected void checkLogin(){
        if(null != UserSharedPreference.getUser(this)){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
    protected User getUserShared(){
       return UserSharedPreference.getUser(this);
    }

    protected void showMessage(String message){
        Toast.makeText(this, message,Toast.LENGTH_LONG).show();
    }

}
