package com.dinsaren.stockmanagementmobileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinsaren.stockmanagementmobileapp.R;
import com.dinsaren.stockmanagementmobileapp.models.res.Category;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>{
    private Context context;
    private List<Category> categoryList;

    public CategoryAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(context).inflate(
               R.layout.category_card_item_layout,null,false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category = categoryList.get(position);
        if(null != category.getName()){
            holder.nameEn.setText(category.getName());
        }
        if(null != category.getNameKh()){
            holder.nameKh.setText(category.getNameKh());
        }

    }

    @Override
    public int getItemCount() {
        if(categoryList.isEmpty()) {
            return 0;
        }
        return categoryList.size();
    }

    // View Holder
    public static class CategoryViewHolder extends RecyclerView.ViewHolder{
        ImageView categoryImage, categoryEdit, categoryDelete;
        TextView nameEn, nameKh;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryImage = itemView.findViewById(R.id.ivCategoryImage);
            categoryEdit = itemView.findViewById(R.id.ivCategoryEdit);
            categoryDelete = itemView.findViewById(R.id.ivCategoryDelete);
            nameEn = itemView.findViewById(R.id.tvCategoryNameEn);
            nameKh = itemView.findViewById(R.id.tvCategoryNameKh);
        }
    }
}
