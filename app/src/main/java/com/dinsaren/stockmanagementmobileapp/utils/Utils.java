package com.dinsaren.stockmanagementmobileapp.utils;

import com.dinsaren.stockmanagementmobileapp.apis.SMSAPIs;
import com.dinsaren.stockmanagementmobileapp.apis.SMSClientAPIs;


public class Utils {

    public static SMSAPIs getClientAPIs(){
        return  SMSClientAPIs.getSMSClientAPIs().create(SMSAPIs.class);
    }

}
