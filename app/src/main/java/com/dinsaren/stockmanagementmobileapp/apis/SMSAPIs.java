package com.dinsaren.stockmanagementmobileapp.apis;

import com.dinsaren.stockmanagementmobileapp.models.req.CategoryReq;
import com.dinsaren.stockmanagementmobileapp.models.req.LoginReq;
import com.dinsaren.stockmanagementmobileapp.models.res.BaesRes;
import com.dinsaren.stockmanagementmobileapp.models.res.CategoryRes;
import com.dinsaren.stockmanagementmobileapp.models.res.LoginRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SMSAPIs {
    @POST("/api/oauth/token")
    Call<LoginRes> login(@Body LoginReq req);

    @GET("/api/app/category/list")
    Call<CategoryRes> getAllCategory(@Header("Authorization") String token);

    @POST("/api/app/category/create")
    Call<BaesRes> createCategory(@Header("Authorization") String token, @Body CategoryReq req);

}
