package com.dinsaren.stockmanagementmobileapp.presenters;

import com.dinsaren.stockmanagementmobileapp.models.req.CategoryReq;
import com.dinsaren.stockmanagementmobileapp.models.res.BaesRes;
import com.dinsaren.stockmanagementmobileapp.utils.Utils;
import com.dinsaren.stockmanagementmobileapp.views.FormCategoryView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormCategoryPresenter {
    private FormCategoryView view;

    public FormCategoryPresenter(FormCategoryView view) {
        this.view = view;
    }

    public void createCategory(String name, String nameKh, String token){
        view.onLoading("");
        CategoryReq req = new CategoryReq();
        req.setId(0);
        req.setName(name);
        req.setNameKh(nameKh);
        req.setStatus("ACT");
        Call<BaesRes> callCreateCategory = Utils.getClientAPIs().createCategory(token,req);
        callCreateCategory.enqueue(new Callback<BaesRes>() {
            @Override
            public void onResponse(Call<BaesRes> call, Response<BaesRes> response) {
                view.onHideLoading("");
                if(response.isSuccessful() && null != response.body()){
                    view.onCreateSuccess();
                }else{
                    view.onError("Create Category Error");
                }
            }

            @Override
            public void onFailure(Call<BaesRes> call, Throwable t) {
                view.onHideLoading("");
                view.onServerError(t.getLocalizedMessage());
            }
        });
    }
}
